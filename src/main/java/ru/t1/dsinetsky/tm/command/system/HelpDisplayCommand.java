package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

import java.util.Collection;

public final class HelpDisplayCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = ArgumentConst.CMD_HELP;

    public static final String NAME = TerminalConst.CMD_HELP;

    public static final String DESCRIPTION = "Shows this message";

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
