package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {
    void clearAll();

    List<Task> returnAll();

    List<Task> returnAll(Sort sort);

    List<Task> returnAll(Comparator comparator);

    Task create(String name) throws GeneralException;

    Task create(String name, String desc) throws GeneralException;

    Task findById(String id) throws GeneralException;

    Task findByIndex(int index) throws GeneralException;

    Task removeById(String id) throws GeneralException;

    Task removeByIndex(int index) throws GeneralException;

    Task updateById(String id, String name, String desc) throws GeneralException;

    Task updateByIndex(int index, String name, String desc) throws GeneralException;

    Task changeStatusById(String id, Status status) throws GeneralException;

    Task changeStatusByIndex(int index, Status status) throws GeneralException;

    List<Task> returnTasksOfProject(String projectId) throws GeneralException;

    void createTestTasks() throws GeneralException;

}
