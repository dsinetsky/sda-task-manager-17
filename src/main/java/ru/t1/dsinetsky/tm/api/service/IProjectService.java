package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> returnAll();

    List<Project> returnAll(Comparator comparator);

    List<Project> returnAll(Sort sort);

    Project create(String name) throws GeneralException;

    Project create(String name, String desc) throws GeneralException;

    Project add(Project project) throws GeneralException;

    void clearAll();

    Project findById(String id) throws GeneralException;

    Project findByIndex(int index) throws GeneralException;

    Project removeById(String id) throws GeneralException;

    Project removeByIndex(int index) throws GeneralException;

    Project updateById(String id, String name, String desc) throws GeneralException;

    Project updateByIndex(int index, String name, String desc) throws GeneralException;

    Project changeStatusById(String id, Status status) throws GeneralException;

    Project changeStatusByIndex(int index, Status status) throws GeneralException;

    void createTestProjects() throws GeneralException;

}
