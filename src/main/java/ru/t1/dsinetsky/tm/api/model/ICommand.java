package ru.t1.dsinetsky.tm.api.model;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface ICommand {

    void execute() throws GeneralException;

    String getName();

    String getDescription();

    String getArgument();

}
