package ru.t1.dsinetsky.tm.exception.system;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralSystemException extends GeneralException {

    public GeneralSystemException() {
    }

    public GeneralSystemException(final String message) {
        super(message);
    }

    public GeneralSystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralSystemException(final Throwable cause) {
        super(cause);
    }

    public GeneralSystemException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
