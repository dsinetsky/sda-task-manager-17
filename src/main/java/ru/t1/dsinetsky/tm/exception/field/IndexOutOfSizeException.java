package ru.t1.dsinetsky.tm.exception.field;

public final class IndexOutOfSizeException extends GeneralFieldException {

    public IndexOutOfSizeException(final int size) {
        super("Index must be between 1 and " + size + "!");
    }

}
